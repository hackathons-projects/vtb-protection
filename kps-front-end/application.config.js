const BASE_URL = 'http://localhost:8080';

export const AppConfig = {
    url: {
        baseURL: BASE_URL,
        layoutUrl: BASE_URL + '/kps/layout',
        socksJsUrl: BASE_URL + '/stomp'
    },
    wsUpdateTopic: "/topic/kps-layout",
};
