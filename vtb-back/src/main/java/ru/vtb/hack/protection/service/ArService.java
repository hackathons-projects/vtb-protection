package ru.vtb.hack.protection.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import ru.vtb.hack.protection.model.ArMappingDto;
import ru.vtb.hack.protection.model.PointDto;

import java.security.SecureRandom;

@RequiredArgsConstructor
@Setter
@Getter
@Service
public class ArService {

    private final FrontEndService frontEndService;
    private SecureRandom secureRandom = new SecureRandom();
    private ArMappingDto currentMapping = new ArMappingDto(getX(), getY());
    private StringBuilder context = new StringBuilder();

    public void generate() {
        currentMapping = new ArMappingDto(getX(), getY());
        frontEndService.sendArMapping(currentMapping);
    }

    public void process(Integer digit) {
        switch (digit) {
            case -2:
                System.out.println(context.toString());
                System.out.println();
                context = new StringBuilder();
            case -1:
                if (context.length() > 0) {
                    System.out.println("DELETE");
                    context.deleteCharAt(context.length() - 1);
                }
            default:
                System.out.println(digit);
                context.append(digit.toString());
        }
    }

    public Integer recognize(PointDto pointDto) {
        float x = pointDto.relativeX();
        float y = pointDto.relativeY();
        float[] xx = currentMapping.getX();
        float[] yy = currentMapping.getY();
        int indexX = x >= xx[0] && x < xx[1] ? 1 : x >= xx[1] ? 2 : 0;
        int indexY = y >= yy[0] && y < yy[1] ? 1 : y >= yy[1] && y < yy[2] ? 2 : y >= yy[2] ? 3 : 0;
        int index = (indexY) * 3 + indexX + 1;
        return currentMapping.getPinPad().get(index - 1);
    }

    private float[] getX() {
        float x1 = generate(0.1f, 0.8f);
        float x2 = generate(x1 + 0.1f, 0.9f);
        return new float[]{x1, x2};
    }

    private float[] getY() {
        float y1 = generate(0.1f, 0.7f);
        float y2 = generate(y1 + 0.1f, 0.8f);
        float y3 = generate(y2 + 0.1f, 0.9f);
        return new float[]{y1, y2, y3};
    }

    private float generate(float rangeMin, float rangeMax) {
        return rangeMin + (rangeMax - rangeMin) * secureRandom.nextFloat();
    }
}
