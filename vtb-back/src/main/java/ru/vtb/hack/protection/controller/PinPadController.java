package ru.vtb.hack.protection.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.vtb.hack.protection.model.ArMappingDto;
import ru.vtb.hack.protection.model.PointDto;
import ru.vtb.hack.protection.service.ArService;

@RequiredArgsConstructor
@RequestMapping("/")
@RestController
public class PinPadController {

    private final ArService arService;

    @PostMapping
    public void index() {
        arService.generate();
    }

    @CrossOrigin("*")
    @PostMapping(value = "/send")
    public void createNewSession(@RequestBody PointDto pointDto) {
        Integer recognized = arService.recognize(pointDto);
        arService.process(recognized);
        arService.generate();
    }

    @CrossOrigin("*")
    @GetMapping("/mapping")
    public ArMappingDto getMapping() {
        arService.generate();
        return arService.getCurrentMapping();
    }

}
