package ru.vtb.hack.protection.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PointDto {

    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;

    public float relativeX(){
        return (float)x/(float)width;
    }

    public float relativeY(){
        return (float)y/(float)height;
    }
}
