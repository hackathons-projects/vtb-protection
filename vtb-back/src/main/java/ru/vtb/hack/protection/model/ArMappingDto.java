package ru.vtb.hack.protection.model;

import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class ArMappingDto {
    final private float[] x;
    final private float[] y;
    final private List<Integer> pinPad;

    public ArMappingDto(float[] x, float[] y) {
        this.x = x;
        this.y = y;
        pinPad = IntStream.range(-2, 10).boxed().collect(Collectors.toList());
        Collections.shuffle(pinPad);
    }

}
