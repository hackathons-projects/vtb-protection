package ru.vtb.hack.protection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtbBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(VtbBackApplication.class, args);
	}

}
