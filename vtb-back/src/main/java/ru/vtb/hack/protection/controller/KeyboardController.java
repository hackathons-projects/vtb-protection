package ru.vtb.hack.protection.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.vtb.hack.protection.service.KeyboardService;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/kps")
@Controller
public class KeyboardController {

    private final KeyboardService keyboardService;

    @GetMapping
    public String index() {
        return "index";
    }

    @ResponseBody
    @GetMapping("/layout")
    public String layout() {
        return keyboardService.generate();
    }

    @ResponseBody
    @PostMapping("/send")
    public void decrypt(@RequestBody String input) {
        String decrypt = keyboardService.decrypt(input);
        System.out.println(decrypt);
    }

}
