package ru.vtb.hack.protection.service;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ru.vtb.hack.protection.config.StompConfig;
import ru.vtb.hack.protection.model.ArMappingDto;

@RequiredArgsConstructor
@Service
public class FrontEndService {

    private final StompConfig stompConfig;
    private final SimpMessagingTemplate webSocket;

    public void sendArMapping(ArMappingDto arMappingDto) {
        webSocket.convertAndSend(stompConfig.getArMappingTopic(), arMappingDto);
    }

}
