package ru.vtb.hack.protection.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("stomp")
@Data
@Component
public class StompConfig {
    private String arMappingTopic;
    private String kspTopic;
    private String endpoint;
    private String destinationPrefix;
    private String cors;
}
