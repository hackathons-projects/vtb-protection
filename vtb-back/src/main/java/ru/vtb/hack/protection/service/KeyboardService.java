package ru.vtb.hack.protection.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public class KeyboardService {

    private String template = "{[escape]:}{[f1]:}{[f2]:}{[f3]:}{[f4]:}{[f5]:}{[f6]:}{[f7]:}{[f8]:}{[f9]:}{[f11]:}{[f12]:}{[print screen]:}{[insert]:}{[delete]:}|" +
            "{[back quote]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[minus]:}{[equals]:}{[backspace]:}|" +
            "{[tab]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[open bracket]:}{[close bracket]:}{[back slash]:}|" +
            "{[caps lock]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[semicolon]:}{[quote]:}{[enter]:}|" +
            "{[shift]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[%s]:}{[comma]:}{[period]:}{[slash]:}{[shift]:}|" +
            "{[ctrl]:}{[fn]:}{[meta]:}{[alt]:}{[space]:}{[alt]:}{[ctrl]:}";
    private List<String> basis = new ArrayList<>();
    private List<String> mapping;

    @PostConstruct
    private void init() {
        for (char c : "1234567890qwertyuiopasdfghjklzxcvbnm".toCharArray()) {
            basis.add("" + c);
        }
        mapping = new ArrayList<>(basis);
        Collections.shuffle(mapping);
        System.out.println(String.format(template, mapping.toArray()));
    }

    public String generate() {
        Collections.shuffle(mapping);
        return String.format(template, mapping.toArray());
    }

    public String decrypt(String input) {
        StringBuilder result = new StringBuilder();
        for (char c : input.toCharArray()) {
            result.append(map("" + c));
        }
        return result.toString();
    }

    public String map(String key) {
        return basis.get(mapping.indexOf(key));
    }
}
