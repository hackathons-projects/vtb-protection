import logging
import sys

from flask import Flask, request
from service import *

app = Flask(__name__)

HOST = '127.0.0.1'
PORT = 5000

logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


@app.route('/sound/train', methods=['POST'])
def train():
    file = request.files['file']
    pna = request.form['pan']
    if file and fileStorageService.validate_file(file.filename):
        _id = fileStorageService.file_converter(file)
        _file_path = fileStorageService.file_path_by_id(_id, 'wav')
        [signal,rate] = soundFeatureService.get_audio(_file_path)
        print('write_dict started')
        CleanWavFiles.write_dict(rate, signal, pna, _id)
        print('CleanWavFiles started')
        CleanWavFiles.create_clean_samples(rate= rate, signal = signal,pna= pna)
        Predict.build_prediction(pna)



        # todo train sound call VAD and mfcc services
        # print('outlier_suppression Started')
        # x = vadService.outlier_suppression(x,fs)
        # print('compute_power_arrays Started')
        # cpa = vadService.compute_power_arrays(x, fs)
        # signal_power_array, noise_power_array = cpa[0], cpa[1]
        # print('gaus Started')
        # g = vadService.gaus(signal_power_array, noise_power_array)
        # signal_power_array, noise_power_array = g[0], g[1]
        # print('phrase_detection Started')
        # fd = vadService.phrase_detection(signal_power_array, noise_power_array)
        # phrase, phrase_detection = fd[0], fd[1]
        # print('phrase_concat Started')
        # pc = vadService.phrase_concat(phrase, phrase_detection)
        # print('phrase_power_threshold Started')
        # vadService.phrase_power_threshold(pc[0],pc[1])

        return 'RESULT'

# Health route
@app.route("/health", methods=['GET'])
def health():
    logging.info(testService.test())
    logging.info(testService.get_context())
    return "I'm alive"


# System shutdown route
@app.route('/shutdown', methods=['POST'])
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'


if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
