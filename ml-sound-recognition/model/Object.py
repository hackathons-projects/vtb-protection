import json
import io
import csv


class Object:
    def to_csv(self):
        output = io.StringIO()
        writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(self.__dict__.keys())
        writer.writerow(self.__dict__.values())
        return output.getvalue()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)