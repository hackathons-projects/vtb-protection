import librosa
import numpy as np
import model as model
from scipy.io import wavfile

class SoundFeatureService:

    def __init__(self):
        pass

    @staticmethod
    def get_audio(audio_path):
        return librosa.load(audio_path)

    def get_features(self, audio_path):
        y, sr = librosa.load(audio_path)

        result = model.Object()
        self.__fill_spectral_features(result, y, sr)
        self.__fill_rythm_features(result, y, sr)

        return result

    def __fill_spectral_features(self, result, y, sr):
        S = np.abs(librosa.stft(y))

        #  Compute a chromagram from a waveform or power spectrogram.
        chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'chroma_stft', chroma_stft, 2)

        # Constant-Q chromagram
        chroma_cqt = librosa.feature.chroma_cqt(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'chroma_cqt', chroma_cqt, 2)

        # Computes the chroma variant “Chroma Energy Normalized” (CENS)
        chroma_cens = librosa.feature.chroma_cens(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'chroma_cens', chroma_cens, 2)

        # Spectral centroid
        spectral_centroid = librosa.feature.spectral_centroid(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'spectral_centroid', spectral_centroid, 2)

        # Compute a mel-scaled spectrogram
        melspectrogram = librosa.feature.melspectrogram(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'melspectrogram', melspectrogram, 2)

        # Mel-frequency cepstral coefficients (MFCCs)
        mfcc = librosa.feature.mfcc(y=y, sr=sr).tolist()
        for index in range(1, len(mfcc) - 1):
            mfcc_part = mfcc[index]
            result.__dict__['mfcc_' + str(index) + '_max'] = max(mfcc_part)
            result.__dict__['mfcc_' + str(index) + '_min'] = min(mfcc_part)

        # Compute root-mean-square (RMS) value for each frame, either from the audio samples y or from a spectrogram S
        rms = librosa.feature.rms(y=y)
        self.__fill_feature_and_derivatives(result, 'rms', rms, 2)

        # Compute p’th-order spectral bandwidth.
        spectral_bandwidth = librosa.feature.spectral_bandwidth(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'spectral_bandwidth', spectral_bandwidth, 2)

        # Compute spectral contrast
        spectral_contrast = librosa.feature.spectral_contrast(S=S, sr=sr)
        self.__fill_feature_and_derivatives(result, 'spectral_contrast', spectral_contrast, 2)

        # Compute spectral flatness
        spectral_flatness = librosa.feature.spectral_flatness(y=y)
        self.__fill_feature_and_derivatives(result, 'spectral_flatness', spectral_flatness, 2)

        # Compute roll-off frequency
        spectral_rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'spectral_rolloff', spectral_rolloff, 2)

        # Get coefficients of fitting an nth-order polynomial to the columns of a spectrogram
        for index in range(5):
            poly_features = librosa.feature.poly_features(S=S, order=index)  # .tolist()[0]
            result.__dict__['poly_features_' + str(index) + '_max'] = poly_features.max()
            result.__dict__['poly_features_' + str(index) + '_min'] = poly_features.min()

        # Computes the tonal centroid features (tonnetz)
        tonnetz = librosa.feature.tonnetz(y=y, sr=sr)
        self.__fill_feature_and_derivatives(result, 'tonnetz', tonnetz, 2)

        # Compute the zero-crossing rate of an audio time series
        zero_crossing_rate = librosa.feature.zero_crossing_rate(y)
        self.__fill_feature_and_derivatives(result, 'zero_crossing_rate', zero_crossing_rate, 2)

    def __fill_rythm_features(self, result, y, sr):
        hop_length = 512

        # Compute the tempogram: local autocorrelation of the onset strength envelope
        onset_strength = librosa.onset.onset_strength(y=y, sr=sr, hop_length=hop_length)
        self.__fill_feature_and_derivatives(result, 'onset_strength', onset_strength, 2)

        tempogram = librosa.feature.tempogram(onset_envelope=onset_strength, sr=sr, hop_length=hop_length)[0]
        self.__fill_feature_and_derivatives(result, 'tempogram', tempogram, 2)

        # Compute global onset autocorrelation
        autocorrelate = librosa.autocorrelate(onset_strength, max_size=tempogram.shape[0])
        self.__fill_feature_and_derivatives(result, 'autocorrelate', autocorrelate, 2)

        autocorrelate_normalized = librosa.util.normalize(autocorrelate)
        self.__fill_feature_and_derivatives(result, 'autocorrelate_normalized', autocorrelate_normalized, 2)

        # Estimate the global tempo for display purposes
        result.__dict__['tempo'] = librosa.beat.tempo(onset_envelope=onset_strength, sr=sr, hop_length=hop_length)[0]

    def __fill_feature_and_derivatives(self, result, feature_name, feature, derivatives_count):
        self.__fill_feature_properties(result, feature_name, feature)
        for index in range(1, derivatives_count + 1):
            derivative = librosa.feature.delta(feature, order=index)
            self.__fill_feature_properties(result, feature_name + '_' + str(index) + '_derivative', derivative)

    @staticmethod
    def __fill_feature_properties(result, feature_name, feature):
        result.__dict__[feature_name + '_mean'] = float(feature.mean())
        result.__dict__[feature_name + '_min'] = float(feature.min())
        result.__dict__[feature_name + '_max'] = float(feature.max())


component = SoundFeatureService()
