import math as m
import numpy.linalg as nl
import numpy as np
import scipy.signal as ss
from scipy.signal import gaussian
from BaselineRemoval import BaselineRemoval
import scipy as sp
import scipy.ndimage
from .Classes import Phrase


class VadService:

    def __init__(self):
        pass

    @staticmethod
    def outlier_suppression(x, fs):

        buffer_duration = 0.1
        accumulation_time = 3
        overlap = 50
        outlier_threshold = 7

        buffer_size = round(((1 + 2 * (overlap / 100)) * buffer_duration) * fs)
        sample_size = round(buffer_duration * fs)

        y = x
        cumm_power = 0
        amount_iteration = m.floor(len(y) / sample_size)

        for iter in range(1, amount_iteration):

            start_index = (iter - 1) * sample_size
            start_index = max(round(start_index - (overlap / 100) * sample_size), 1)
            end_index = min(start_index + buffer_size, len(x))

            buffer = y[start_index: end_index]
            power = nl.norm(buffer)

            if ((accumulation_time / buffer_duration < iter) & ((outlier_threshold * cumm_power / iter) < power)):
                buffer = buffer * (1 - ss.tukeywin(end_index - start_index + 1, 0.8)) #tukeywin - scipy.signal
                y[start_index: end_index] = buffer

                cumm_power = cumm_power + (cumm_power / iter)

            cumm_power = cumm_power + power
        return y

    @staticmethod
    def compute_power_arrays(x, fs):

        buffer_duration = 10 * 10 ** (-3)
        overlap = 50

        sample_size = round(buffer_duration * fs)
        buffer_size = round(((1 + 2 * (overlap / 100)) * buffer_duration) * fs)

        signal_power_array = np.zeros([m.floor(len(x) / sample_size), 1])
        noise_power_array = np.zeros([len(signal_power_array), 1])

        for iter in range(1, len(signal_power_array)):
            start_index = (iter - 1) * sample_size
            start_index = max(round(start_index - (overlap / 100) * sample_size), 1)
            end_index = min(start_index + buffer_size, len(x))

            buffer = x[start_index: end_index]
            signal_power_array[iter] = nl.norm(buffer)
            noise_power_array[iter] = nl.norm(abs(np.diff(np.sign(buffer))))

        return [signal_power_array, noise_power_array]

    @staticmethod
    def gaus(signal_power_array, noise_power_array):

        polynomial_degree = 2

        signal_power_array = sp.ndimage.gaussian_filter(signal_power_array, sigma=2, truncate=4)
        baseObj = BaselineRemoval(signal_power_array)
        signal_power_array = baseObj.ModPoly(polynomial_degree)
        signal_power_array = signal_power_array / signal_power_array.max() if signal_power_array is not None else 0


        noise_power_array = sp.ndimage.gaussian_filter(noise_power_array, sigma=2, truncate=4)
        baseObj = BaselineRemoval(noise_power_array)
        noise_power_array = baseObj.ModPoly(polynomial_degree)
        if noise_power_array is not None:
            noise_power_array = noise_power_array / noise_power_array.max()
            noise_power_array = -noise_power_array + noise_power_array.max()
            noise_power_array = noise_power_array / noise_power_array.max()
            noise_power_array = (1 - noise_power_array)
        else:
            noise_power_array = 0
        return [signal_power_array, noise_power_array]

    @staticmethod
    def phrase_detection(signal_power_array, noise_power_array):
        phrase_detection = np.zeros(np.size(signal_power_array))
        max_magn = 0
        threshold = 1

        for i in range(len(signal_power_array)):
            max_magn = max(max_magn, signal_power_array[i])
            if (signal_power_array[i] < 2 * threshold * max_magn):
                phrase_detection[i] = 1
                max_magn = 0

        #  find the start of phrases
        max_magn = 0
        for iter in range(len(signal_power_array), -1, 1):
            max_magn = max(max_magn, signal_power_array[iter])
            if (signal_power_array[iter] < 2 * threshold * max_magn):
                phrase_detection[iter] = 1
                max_magn = 0

        # post processing of phrase detection array
        phrase_detection = list(reversed(phrase_detection))
        phrase_detection[0] = 0
        phrase_detection[len(phrase_detection) - 1] = 0
        edge_phrase = np.diff(phrase_detection)


    #  create phrase array
        phrase_counter = 0
        phrase = []
        for iter in range(len(edge_phrase) - 1):
            if (edge_phrase[iter] > 0):
                #p.start = iter
                phrase.append(Phrase(iter))
                #phrase_counter = phrase_counter + 1
            elif (edge_phrase[iter] < 0):
                phrase[iter - 1].end = iter
                start = phrase[iter - 1].start
                phrase[iter - 1].power = np.mean(signal_power_array[start: iter])
                phrase[iter - 1].noise_power = np.mean(noise_power_array[start: iter])
        return [phrase,phrase_detection]

    @staticmethod
    def phrase_concat(phrase, phrase_detection):
        length_threshold = 16
        threshold = 1
        iter = 1
        while (iter < len(phrase)):
            phrase_length = phrase[iter].end - phrase[iter].start
            print(1)
            print(phrase)
            if ((phrase_length < length_threshold) &  (iter < len(phrase)) &  (iter >= 1)):

                lookup_power = phrase[iter + 1].power
                behind_power = phrase[iter - 1].power

                if (abs(lookup_power - phrase[iter].power) < abs(behind_power - phrase[iter].power)):
                    next_iter = iter + 1
                else:
                    next_iter = iter - 1

                power_ratio = phrase[next_iter].power / phrase[iter].power
                power_ratio = min(power_ratio, 1 / power_ratio)
                if (power_ratio < threshold):
                    iter = iter + 1

                min_index = min(phrase[iter].start, phrase[next_iter].start)
                max_index = max(phrase[iter].end, phrase[next_iter].end)

                phrase_next_length = (phrase[next_iter].end - phrase[next_iter].start)

                phrase[next_iter].power = ((phrase[iter].power * phrase_length) +
                                           (phrase[next_iter].power * phrase_next_length)) / (phrase_length +
                                                                                              phrase_next_length)

                phrase[next_iter].noise_power = ((phrase[iter].noise_power * phrase_length)
                                                 + phrase[next_iter].noise_power * phrase_next_length) / (phrase_length
                                                                                                          + phrase_next_length)
                print(2)
                print(phrase)
                phrase_detection[(min_index + 1): (max_index - 1)] = phrase_detection[phrase[next_iter].start + 1]

                phrase[next_iter].start = min_index
                phrase[next_iter].end = max_index
            else:
                iter = iter + 1
        return [phrase, phrase_detection]

    @staticmethod
    def phrase_power_threshold(phrase, phrase_detection):
        print(1)
        phrase_power_threshold = 0.1
        iter = 1
        print(len(phrase))
        print(phrase)
        while (iter <= len(phrase) - 1):
            print(2)
            if (phrase_power_threshold < phrase[iter].power):
                phrase[iter].voice = 1
            elif ((phrase_power_threshold < 5*phrase[iter].power) & (phrase[iter].noise_power
                                                                     < 0.5*phrase[iter].power)):
                phrase[iter].voice = 1
            else:
                phrase[iter].voice = 0

            if (iter >= 1):
                if phrase[iter-1].voice == phrase[iter].voice:
                    phrase[iter-1].end = phrase[iter].end
                    phrase_detection[(phrase[iter-1].start+1) : (phrase[iter-1].end-1)] = phrase[iter-1].voice
                    phrase[iter] = []
            iter = iter + 1

            print(phrase, phrase_detection)
        return [phrase, phrase_detection]

        # buffer_duration = 0.1
        # sample_size = round(buffer_duration * fs)
        #
        # for iter  in range(1, len(phrase)):
        #     start_index = (phrase[iter].start - 1) * sample_size + 1
        #     end_index = phrase[iter].end * sample_size
        #     phrase[iter].start = start_index
        #     phrase[iter].end = end_index
        #
        # phrase[1].start = 1
        # phrase[len(phrase)-1].end = len(phrase)
        # return [phrase, phrase_detection]



        # print(3)
        #
        # spread_voice = round(50 * 10 ^ (-3) * fs)
        # iter = 2
        # while (iter < len(phrase)):
        #     if (phrase[iter].voice):
        #         phrase[iter].start = phrase[iter].start - spread_voice
        #         phrase[iter].end = phrase[iter].end + spread_voice
        #     else:
        #         phrase[iter].start = phrase[iter - 1].end + 1
        #         phrase[iter].end = phrase[iter + 1].start - 1 - spread_voice
        #
        #     phrase[iter].start = max(phrase[iter].start, 1)
        #     phrase[iter].end = min(phrase[iter].end, len(phrase))
        #
        #     if (phrase[iter].start < phrase[iter].end):
        #         iter = iter + 1
        #     else:
        #         phrase[iter - 1].end = phrase[iter].end
        #         phrase[iter] = []
        #
        # end = len(phrase) - 1
        # phrase[1].start = 1
        # phrase[1].end = phrase[2].start - 1
        # phrase[end].start = phrase[end - 1].end + 1
        # phrase[end].end = len(phrase)





component = VadService()