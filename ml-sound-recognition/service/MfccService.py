import math
import numpy as np
from scipy import signal
from scipy.ndimage import gaussian_filter
import statistics

class MfccService:

    def __init__(self):
        pass

    @staticmethod
    def __log_index_space(amount_point, input_length, decimation_speed):
    # создаем массив индексов, значение которого
    # логарифмически распределено по всему пространству индексов исходного спектра.
        index_space = math.exp((range(amount_point - 1) / (amount_point - 1)) * decimation_speed)
        index_space = index_space - index_space[1]
        index_space = index_space / index_space[len(index_space)]
        index_space = round((input_length - 1) * index_space + 1)
        return index_space

    @staticmethod
    def __decimation_spectrum(freq_signal, index_space):
    # После составления массива индексов, единственное, что остается, это выбрать нужные точки
    # исходного спектра и аккумулировать среднюю энергию между точками.
    # децимация спектра
        y = np.zeros(np.size(index_space))
        min_value = min(freq_signal)
        freq_local = [x - min_value for x in freq_signal]
        for i in range(2, (len(index_space) - 1)):
            index_frame = index_space[(i - 1): (i + 1)]
            frame = freq_local[index_frame]

            if (4 < len(index_frame)):
                gw = signal.gaussian(round(len(index_frame) / 2), 1)
                frame = gaussian_filter(gw, 1, frame) / (round(len(index_frame)) / 4)
            y[i] = statistics.median()
        y[1] = statistics.mean(y)
        y[len(y)] = y[len(y) - 1]
        y = y + min_value
        return y

    def eject_spectr(self, sample_length, sample, fs, band_pass):
        # set parameters
        band_start = band_pass[1]
        band_stop = band_pass[2]
        decimation_speed = 2.9
        amount_point = 800

        # create spaces
        spectrum_space = range(0, fs / len(sample), (fs / 2) - (1 / len(sample)))
        spectrum_space = filter(lambda x: x <= band_stop, spectrum_space)
        sample_index_space = self.__log_index_space(amount_point, len(spectrum_space), decimation_speed)

        # create spectral density
        sample_spectr = 10 * np.log10(abs(np.fft(sample, len(sample))) / (len(sample) * sample_length))
        sample_spectr = sample_spectr[1: len(spectrum_space)]
        sample_spectr = self.__decimation_spectrum(sample_spectr, sample_index_space)
        return sample_spectr  # ,log_space?

    # Построение LogLog спектрограммы
    # spetrogramm

    buffer_duration = 0.1 # in seconds
    overlap = 50 # in percent

    fs = 1; y = [1,2,3] #for examle
    band_pass = range(-20, 20) #for examle
    sample_size = round(buffer_duration * fs)
    buffer_size = round(((1 + 2 * (overlap / 100)) * buffer_duration) * fs)
    [temp, log_space] = eject_spectr(buffer_size, range(1, buffer_size), fs, band_pass)
    time_space = range(0, sample_size / fs, len(y) / fs - 1 / (fs))
    [temp, log_space] = eject_spectr(buffer_size, range(1, buffer_size), fs, band_pass)

    # array of power buffer samples
    db_range = 25
    spectrum_array = np.zeros(len(time_space), len(log_space))
    for i in range(1, len(time_space)):
        start_index = (i - 1) * sample_size
        start_index = max(round(start_index - (overlap / 100) * sample_size), 1)
        end_index = min(start_index + buffer_size, len(y))

        phrase_space = phrase_space[start_index: end_index]

        buffer = y(phrase_space) / max(y(phrase_space))
        buffer = buffer * np.kaiser(len(buffer), 4)

        spectr = eject_spectr(buffer_size, buffer, fs, band_pass)
        spectr = max(spectr - (max(spectr) - db_range), 0)
        spectrum_array[iter, 1: len(log_space)] = spectr

    win_len = 11
    win = signal.gaussian(win_len, 2)
    win = win / max(win)
    spectrum_array = signal.convolve2d(win, spectrum_array) / (win_len * 2)

    # посчитать оценки МО и СКО для каждой частотной компоненты,
    # а после объединить в один вектор главных компонент
    std_spectr = np.std(spectrum_array, 0, 1)
    mean_spectr = statistics.mean(spectrum_array, 1);

    principal_component = signal.cat(2, mean_spectr, std_spectr)

component = MfccService()