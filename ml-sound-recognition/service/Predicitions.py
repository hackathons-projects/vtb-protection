from tensorflow.keras import layers
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
import os
import pandas as pd
import  numpy as np
from tqdm import tqdm
from scipy.io import wavfile
from python_speech_features import mfcc, logfbank
from sklearn.utils.class_weight import compute_class_weight
import pickle
from tensorflow.keras.callbacks import ModelCheckpoint


class Config:
  def __init__(self, mode='conv', nfilt=26, nfeat=13, nfft=1024, rate=22000):
    self.mode = mode
    self.nfilt = nfilt
    self.nfeat = nfeat
    self.nfft = nfft
    self.rate = rate
    self.step = int(rate / 10)
    self.model_path = os.path.join('models', 'cnn.model')
    self.p_path = os.path.join('pickles', 'cnn.p')


class Predict():

    @staticmethod
    def build_prediction(pna):
        p_path = os.path.join('pickles','cnn.p')
        with open(p_path, 'rb') as handler:
            config = pickle.load(handler)


        y_true = []
        y_pred = []
        fn_prob = []
        audio_dir = '/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/CleanSamples2'
        model_dir = '/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/models/cnn.model'
        print('Extractiong Features from audio')
        for fn in tqdm(os.listdir(audio_dir)):
            rate, wav = wavfile.read(os.path.join(audio_dir, fn))
            label = 'client'
            c = '777'
            y_prob = []
            model = tf.load_model(model_dir)

            for i in range(0, wav.shape[0] - config.step, config.step):
                sample = wav[i : i+config.step]
                x = mfcc(sample, rate, numcep=config.nfeat, nfft= config.nfft, nfilt=config.nfilt)
                x = (x - config.min) / (config.max - config.min)
                x = x.reshape(1, x.shape[0], x.shape[1], 1)

                y_hat = model.predict
