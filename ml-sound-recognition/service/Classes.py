import os

class Phrase:
  def __init__(self,start):
    self.start = start
    # self.end = end
    # self.end = power
    # self.end = noise_power

class Config:
  def __init__(self, mode='conv', nfilt=26, nfeat=13, nfft=1024, rate=22000):
    self.mode = mode
    self.nfilt = nfilt
    self.nfeat = nfeat
    self.nfft = nfft
    self.rate = rate
    self.step = int(rate / 10)
    self.model_path = os.path.join('models', 'cnn.model')
    self.p_path = os.path.join('pickles', 'cnn.p')
