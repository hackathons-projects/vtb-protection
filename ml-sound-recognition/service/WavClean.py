#import matplotlib.pyplot as plt
from scipy.io import wavfile
#import argparse
import os
from glob import glob
import numpy as np
from librosa.core import resample, to_mono
from tqdm import tqdm
import pandas as pd
from python_speech_features import mfcc, logfbank

class CleanWavFiles:

    @staticmethod
    def write_dict(rate, signal,pna, id):
        with open("text2.txt", "a+") as testfile1:
            testfile1.write(str(signal.shape[0]/rate) + ',' + pna + ',' + id + '\n')

    @classmethod
    def create_clean_samples(cls,rate, signal,pna):
        print('c1')
        signals = {}
        fft = {}
        fbank = {}
        mfccs = {}
        mask, y_mean = cls.envelope(signal, rate, 0.005)
        print('cleanSample/'+ str(pna)+'.wav')
        wavfile.write('cleanSamples2/'+ str(pna)+'.wav', rate = rate, data = signal[mask])
        #signal = signal[mask]
        #
        # signals.append({pna: signal})
        # fft.append({pna: cls.calc_fft(signal, rate)})
        #
        # bank = logfbank(signal[:rate], rate, nfilt= 26, nfft= 1103).T
        # fbank.append({pna: bank})
        #
        # mel = mfccs(signal[:rate], rate, numcep = 13, nfil = 26, nfft= 1103).T
        # mfccs.append({pna: mel})



    @staticmethod
    def calc_fft(signal, rate):
        n = len(signal)
        freq = np.fft.rfftfreq(n, d = 1/rate)
        Y = abs(np.fft.rfft(signal/n))
        return (Y,freq)


    @staticmethod
    def envelope(y, rate, threshold):
        mask = []
        y = pd.Series(y).apply(np.abs)
        y_mean = y.rolling(window=int(rate/20),
                           min_periods=1,
                           center=True).max()
        for mean in y_mean:
            print(str(mean)+' is in process')
            if mean > threshold:
                mask.append(True)
            else:
                mask.append(False)
        return mask, y_mean

    @staticmethod
    def downsample_mono(path, sr):
        rate, wav = wavfile.read(path)
        wav = wav.astype(np.float32, order='F')
        try:
            tmp = wav.shape[1]
            wav = to_mono(wav.T)
        except:
            pass
        wav = resample(wav, rate, sr)
        wav = wav.astype(np.int16)
        return sr, wav

    @staticmethod
    def save_sample(sample, rate, target_dir, fn, ix):
        fn = fn.split('.wav')[0]
        dst_path = os.path.join(target_dir.split('.')[0], fn+'_{}.wav'.format(str(ix)))
        if os.path.exists(dst_path):
            return
        wavfile.write(dst_path, rate, sample)

    @staticmethod
    def check_dir(path):
        if os.path.exists(path) is False:
            os.mkdir(path)

    def split_wavs(self,src_root,dst_root,delta_time,sr):
        threshold = 0.005
        dt = delta_time

        wav_paths = glob('{}/**'.format(src_root), recursive=True)
        wav_paths = [x for x in wav_paths if '.wav' in x]
        dirs = os.listdir(src_root)
        self.check_dir(dst_root)
        classes = os.listdir(src_root)
        for _cls in classes:
            target_dir = os.path.join(dst_root, _cls)
            self.check_dir(target_dir)
            src_dir = os.path.join(src_root, _cls)
            for fn in tqdm(os.listdir(src_dir)):
                src_fn = os.path.join(src_dir, fn)
                rate, wav = self.downsample_mono(src_fn, sr)
                mask, y_mean = self.envelope(wav, rate, threshold=threshold)
                wav = wav[mask]
                delta_sample = int(dt*rate)

                # cleaned audio is less than a single sample
                # pad with zeros to delta_sample size
                if wav.shape[0] < delta_sample:
                    sample = np.zeros(shape=(delta_sample,), dtype=np.int16)
                    sample[:wav.shape[0]] = wav
                    self.save_sample(sample, rate, target_dir, fn, 0)
                # step through audio and save every delta_sample
                # discard the ending audio if it is too short
                else:
                    trunc = wav.shape[0] % delta_sample
                    for cnt, i in enumerate(np.arange(0, wav.shape[0]-trunc, delta_sample)):
                        start = int(i)
                        stop = int(i + delta_sample)
                        sample = wav[start:stop]
                        self.save_sample(sample, rate, target_dir, fn, cnt)