from .FileStorageService import component as fileStorageService
from .SoundFeatureService import component as soundFeatureService
from .TestService import component as testService
from .VadService import component as vadService
#from .MfccService import component as mfccService
from .WavClean import CleanWavFiles
from .Classes import Config
from .Predicitions import Predict
#from .CNNFeaturesExtraction import FeaturesExtractions
