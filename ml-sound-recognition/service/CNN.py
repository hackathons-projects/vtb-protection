from tensorflow.keras import layers
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
import kapre.time_frequency as ktf
from tensorflow.keras.utils import to_categorical
#import kapre.utils as ku
import tensorflow as tf
import os
import pandas as pd
import  numpy as np
from tqdm import tqdm
import librosa
from scipy.io import wavfile
from python_speech_features import mfcc, logfbank
from sklearn.utils.class_weight import compute_class_weight
import pickle
from tensorflow.keras.callbacks import ModelCheckpoint
from .Classes import Config






def get_conv_model():
    print('m1')
    model = tf.keras.Sequential()
    model.add(layers.Conv2D(16,(3,3), activation = 'relu',
              strides = (1,1), padding = 'same', input_shape = input_shape))
    model.add(layers.Conv2D(32, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(64, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(128, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.MaxPool2D(2,2))
    model.add(layers.Dropout(0.5))
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(2, activation='sigmoid'))
    model.summary()
    model.compile(loss= 'binary_crossentropy', optimizer= 'adam', metrics= ['accuracy'])
    return model



config = Config(mode = 'conv')


checkpoint = ModelCheckpoint(config.model_path, monitor= 'val_acc', verbose= 1, mode = 'max',
                             save_best_only= True, save_weights_only= False, period= 1)

X = np.load('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/Xsample1.npy')
Y = np.load('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/Ysample1.npy')


#X = X.reshape(1,-1)
#Y = np.asarray(Y).astype('float32').reshape((-1,1))

y_flat = np.argmax(Y, axis=1)

input_shape = (X.shape[1], X.shape[2], 1)


model = get_conv_model()


class_weight = compute_class_weight('balanced', np.unique(y_flat), y_flat)

class_weight = {i : class_weight[i] for i in range(len(np.unique(y_flat)))}

model.fit(X, Y, epochs = 10, batch_size = 32, shuffle = True, class_weight = class_weight,
          validation_split=0.25, callbacks = [checkpoint])
model.save(config.model_path)


#
# (36598, 9, 13, 1)
# (36598, 34)