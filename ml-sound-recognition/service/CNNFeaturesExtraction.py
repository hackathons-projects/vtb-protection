from tensorflow.keras import layers
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
import os
import pandas as pd
import  numpy as np
from tqdm import tqdm
from scipy.io import wavfile
from python_speech_features import mfcc, logfbank
from sklearn.utils.class_weight import compute_class_weight
import pickle
from tensorflow.keras.callbacks import ModelCheckpoint
#from .Classes import Config



class Config:
  def __init__(self, mode='conv', nfilt=26, nfeat=13, nfft=1024, rate=22000):
    self.mode = mode
    self.nfilt = nfilt
    self.nfeat = nfeat
    self.nfft = nfft
    self.rate = rate
    self.step = int(rate / 10)
    self.model_path = os.path.join('models', 'cnn.model')
    self.p_path = os.path.join('pickles', 'cnn.p')


print('start')
pan = '777'
df = pd.read_csv('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/text.txt', sep = ',', dtype = 'str')

df['SLength'] = df['SLength'].astype('float')
df.set_index('pan', inplace= True)
df['label'] = 'no'
df.loc[pan,'label'] = 'client'
n_samples = 2 * int(df['SLength'].sum()/0.1)

print(df)

classes = list(np.unique(df.label))
class_dict = df.groupby('pan').SLength.mean()
prob_dist = class_dict/class_dict.sum()


choices = np.random.choice(class_dict.index, p = prob_dist)

print(class_dict)



def check_data():
    if os.path.isfile(config.p_path):
        print('Loading existing data')
        with open(config.p_path, 'rb') as handel:
            tmp = pickle.load(handel)
            return  tmp
    else:
        return  None



def build_rand_feat(df):
    tmp = check_data()
    if tmp:
        return tmp.data[0], tmp.data[1]
    X = []
    y = []
    _min, _max = float('inf'), - float('inf')

    for _ in tqdm(range(n_samples)):
        rand_class = np.random.choice(class_dict.index, p = prob_dist)
        file = np.random.choice(df[df.index == rand_class].index)
        rate, wav = wavfile.read('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/cleanSamples/'+str(file)+'.wav')
        label = df.at[file, 'label']
        rand_index = np.random.randint(0, wav.shape[0] - config.step)
        sample = wav[rand_index : rand_index+config.step]
        X_sample = mfcc(sample, rate, numcep=config.nfeat, nfft= config.nfft, nfilt=config.nfilt)

        _min = min(np.amin(X_sample), _min)
        _max = max(np.amax(X_sample), _max)

        X.append(X_sample)
        y.append(classes.index(label))
    config.min = _min
    config.max = _max
    print('end loop')
    X, y = np.array(X), np.array(y)
    X = (X - _min)/(_max - _min)
    X = X.reshape(X.shape[0],X.shape[1],X.shape[2], 1)

    l = len(np.unique(classes))
    y = to_categorical(y, num_classes=l)

    config.data = (X,y)
    with open(config.p_path, 'wb') as handel:
        pickle.dump(config,handel,protocol=2)

    return X,y

config = Config(mode = 'conv')

X, Y = build_rand_feat(df)

#np.save('Xsample1.npy', X)
#np.save('Ysample1.npy', Y)

print('Data Extracted')
print(X.shape)
print(Y.shape)


def get_conv_model():
    print('m1')
    model = tf.keras.Sequential()
    model.add(layers.Conv2D(16,(3,3), activation = 'relu',
              strides = (1,1), padding = 'same', input_shape = input_shape))
    model.add(layers.Conv2D(32, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(64, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.Conv2D(128, (3, 3), activation='relu',
              strides=(1, 1), padding='same'))
    model.add(layers.MaxPool2D(2,2))
    model.add(layers.Dropout(0.5))
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(2, activation='sigmoid'))
    model.summary()
    model.compile(loss= 'binary_crossentropy', optimizer= 'adam', metrics= ['accuracy'])
    return model



checkpoint = ModelCheckpoint(config.model_path, monitor= 'val_acc', verbose= 1, mode = 'max',
                             save_best_only= True, save_weights_only= False, period= 1)

#X = np.load('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/Xsample1.npy')
#Y = np.load('/Users/sveta/pycharm_tensor_env/vtb-protection/ml-sound-recognition/Ysample1.npy')



y_flat = np.argmax(Y, axis=1)

input_shape = (X.shape[1], X.shape[2], 1)


model = get_conv_model()


class_weight = compute_class_weight('balanced', np.unique(y_flat), y_flat)

class_weight = {i : class_weight[i] for i in range(len(np.unique(y_flat)))}

print('model evaluation')
model.fit(X, Y, epochs = 10, batch_size = 32, shuffle = True, class_weight = class_weight,
          validation_split=0.25, callbacks = [checkpoint])

print('model save')
model.save(config.model_path)