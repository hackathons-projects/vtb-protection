package ru.vtb.ar.services;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import com.divyanshu.draw.widget.DrawView;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.vtb.ar.MainActivity;
import ru.vtb.ar.model.ArMappingDto;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class DrawService {

    private static final String TAG = "DrawService";
    final private MainActivity injected;
    final DrawView drawView;
    final OkHttpClient client = new OkHttpClient();
    private Runnable runnable;
    private Gson gson;
    private ArMappingDto iHateAndroidObject;

    @SuppressWarnings("checkResult")
    public DrawService(MainActivity activity, DrawView drawView) {
        injected = activity;
        this.drawView = drawView;
        this.gson = new Gson();

        Request request = new Request.Builder()
                .url("http://192.168.0.49:8080/mapping")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    // do something wih the result
                    String responseBody = response.body().string();
                    Log.d(TAG, "onResponse: " + responseBody);
                    iHateAndroidObject = gson.fromJson(responseBody, ArMappingDto.class);
                }
            }
        });

        StompClient mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "ws://192.168.0.49:8080/stomp/websocket");
        mStompClient.connect();

        mStompClient.topic("/topic/ar-mapping").subscribe(topicMessage -> {
            Log.d(DrawService.class.getName(), topicMessage.getPayload());
            iHateAndroidObject = gson.fromJson(topicMessage.getPayload(), ArMappingDto.class);
        });

        Handler handlerUi = new Handler();
        injected.runOnUiThread(() -> handlerUi.postDelayed(runnable = () -> {
            if (iHateAndroidObject != null) {
                drawView.clearCanvas();
                drawPinPad(iHateAndroidObject);
            }
            handlerUi.postDelayed(runnable, 500);
        }, 500));
    }


    public void drawPinPad(ArMappingDto data) {
        int X = drawView.getWidth();
        int Y = drawView.getHeight();
        drawLine(0, data.getY()[0] * Y, X, data.getY()[0] * Y);
        drawLine(0, data.getY()[1] * Y, X, data.getY()[1] * Y);
        drawLine(0, data.getY()[2] * Y, X, data.getY()[2] * Y);
        drawLine(data.getX()[0] * X, 0, data.getX()[0] * X, Y);
        drawLine(data.getX()[1] * X, 0, data.getX()[1] * X, Y);

        updateText(injected.text1, (int) mean(0, data.getX()[0] * X), (int) mean(0, data.getY()[0] * Y), data.text(0));
        updateText(injected.text2, (int) mean(data.getX()[0] * X, data.getX()[1] * X), (int) mean(0, data.getY()[0] * Y), data.text(1));
        updateText(injected.text3, (int) mean(data.getX()[1] * X, X), (int) mean(0, data.getY()[0] * Y), data.text(2));

        updateText(injected.text4, (int) mean(0, data.getX()[0] * X), (int) mean(data.getY()[0] * Y, data.getY()[1] * Y), data.text(3));
        updateText(injected.text5, (int) mean(data.getX()[0] * X, data.getX()[1] * X), (int) mean(data.getY()[0] * Y, data.getY()[1] * Y), data.text(4));
        updateText(injected.text6, (int) mean(data.getX()[1] * X, X), (int) mean(data.getY()[0] * Y, data.getY()[1] * Y), data.text(5));

        updateText(injected.text7, (int) mean(0, data.getX()[0] * X), (int) mean(data.getY()[1] * Y, data.getY()[2] * Y), data.text(6));
        updateText(injected.text8, (int) mean(data.getX()[0] * X, data.getX()[1] * X), (int) mean(data.getY()[1] * Y, data.getY()[2] * Y), data.text(7));
        updateText(injected.text9, (int) mean(data.getX()[1] * X, X), (int) mean(data.getY()[1] * Y, data.getY()[2] * Y), data.text(8));

        updateText(injected.text10, (int) mean(0, data.getX()[0] * X), (int) mean(data.getY()[2] * Y, Y), data.text(9));
        updateText(injected.text11, (int) mean(data.getX()[0] * X, data.getX()[1] * X), (int) mean(data.getY()[2] * Y, Y), data.text(10));
        updateText(injected.text12, (int) mean(data.getX()[1] * X, X), (int) mean(data.getY()[2] * Y, Y), data.text(11));
    }

    @SuppressLint("SetTextI18n")
    public void updateText(TextView textView, int x, int y, String text) {
        textView.setPadding(x, y, 0, 0);
        textView.setText(text);
    }

    private int mean(float x1, float x2) {
        return (int) ((x2 - x1) / 2 + x1);
    }

    private void drawLine(float x1, float y1, float x2, float y2) {
        // Dispatch touch event to view
        drawView.dispatchTouchEvent(MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis() + 100,
                MotionEvent.ACTION_DOWN,
                x1,
                y1,
                0)
        );
        drawView.dispatchTouchEvent(MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis() + 100,
                MotionEvent.ACTION_MOVE,
                x2,
                y2,
                0)
        );
        drawView.dispatchTouchEvent(MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis() + 100,
                MotionEvent.ACTION_UP,
                x2,
                y2,
                0)
        );
    }
}
