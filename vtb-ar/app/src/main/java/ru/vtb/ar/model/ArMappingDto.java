package ru.vtb.ar.model;


import java.util.List;

public class ArMappingDto {
    private float[] x;
    private float[] y;
    private List<Integer> pinPad;


    public ArMappingDto() {
    }

    public ArMappingDto(float[] x, float[] y, List<Integer> pinPad) {
        this.x = x;
        this.y = y;
        this.pinPad = pinPad;
    }

    public float[] getX() {
        return x;
    }

    public void setX(float[] x) {
        this.x = x;
    }

    public float[] getY() {
        return y;
    }

    public void setY(float[] y) {
        this.y = y;
    }

    public List<Integer> getPinPad() {
        return pinPad;
    }

    public void setPinPad(List<Integer> pinPad) {
        this.pinPad = pinPad;
    }

    public String text(int index) {
        switch (pinPad.get(index)) {
            case -1:
                return "OK";
            case -2:
                return "Esc";
            default:
                return pinPad.get(index).toString();
        }
    }
}
